import React, { Component } from "react";
import "./renderWithMap.css";
import { kinhArr } from "./data_renderWithMap";
import ItemListkinh from "./ItemListkinh";

export default class RenderWithMap extends Component {
  state = {
    listkinh: kinhArr,
  };
  renderListKinh = () => {
    return this.state.listkinh.map((item, index) => {
      return (
        <ItemListkinh
          kinh={item}
          key={item.id}
          hanldeKinh={this.props.hanldeKinh}
        />
      );
    });
  };
  render() {
    return (
      <div className="container">
        <div className="model">
          <img src="./glassesImage/model.jpg" alt="" className="" />
          <img src="./glassesImage/model.jpg" alt="" className="" />
        </div>
        <div className="anhKinh">{this.renderListKinh()}</div>
      </div>
    );
  }
}
