// import logo from "./logo.svg";
import "./App.css";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import ThayKinh from "./RenderWithMap/ThayKinh";
import React, { Component } from "react";

class App extends Component {
  state = {
    url: "./glassesImage/v7.png",
  };
  hanldeKinh = (url) => {
    console.log("url: ", url);
    this.setState({ url });
  };
  render() {
    return (
      <div className="App">
        <RenderWithMap hanldeKinh={this.hanldeKinh} />
        <ThayKinh url={this.state.url} />
      </div>
    );
  }
}

export default App;
